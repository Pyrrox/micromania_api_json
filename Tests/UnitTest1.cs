using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNetCore.Mvc;
using API_MICROMANIA_JSON.Model;
using API_MICROMANIA_JSON.Controllers;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetMethodTest()
        {
            GamesController c = new GamesController();
            Assert.IsNotNull(c.Index());
        }

        [TestMethod]
        public void PostMethodTest()
        {
            GamesController c = new GamesController();
            var Game = new Game { Id = 99, Name = "GTA", Genre = "OpenWorld" };
            var resultat = c.Create(Game);
            Assert.IsInstanceOfType(resultat, typeof(OkObjectResult));
        }

        [TestMethod]
        public void EditMethodTest()
        {
            GamesController c = new GamesController();
            var Game = new Game { Id = 99, Name = "GTA", Genre = "GTALIKE" };
            var resultat = c.Edit(Game);
            Assert.IsInstanceOfType(resultat, typeof(OkObjectResult));
        }

        [TestMethod]
        public void DeleteMethodTest()
        {
            GamesController c = new GamesController();
            var Game = new Game { Id = 99, Name = "GTA", Genre = "GTALIKE" };
            var resultat = c.Delete(Game);
            Assert.IsInstanceOfType(resultat, typeof(OkObjectResult));
        }
    }
}
